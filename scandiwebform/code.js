/*Type Switcher */

function showDiv(select){
   if(select.value==0){
    document.getElementById('hidden_div').style.display = "none";
    document.getElementById('hidden_div2').style.display = "none";
    document.getElementById('hidden_div3').style.display = "none";
   }
    else if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
    document.getElementById('hidden_div2').style.display = "none";
    document.getElementById('hidden_div3').style.display = "none";
   }else if (select.value==2) {
     document.getElementById('hidden_div').style.display = "none";
     document.getElementById('hidden_div2').style.display = "block";
     document.getElementById('hidden_div3').style.display = "none";
   } else {
    document.getElementById('hidden_div').style.display = "none";
    document.getElementById('hidden_div2').style.display = "none";
    document.getElementById('hidden_div3').style.display = "block";
   }
} 


/*Allows specific insert forms to insert only numbers */
      function isNumberKey(evt)
      {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
        return false;
        return true;
      }  
      
      
      function isNumericKey(evt)
      {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
        return true;
        return false;
      } 